@extends('dockermanager::layouts.master')

@section('content')

	<nav>
  		<ul class="pager">
    		<li class="previous"><a href="{{ url('/')}}"><span aria-hidden="true">&larr;</span> Overview</a></li>
  		</ul>
	</nav>
	
	<div class="table-responsive">
		<table class="table">
			<thead> 
				<tr> 
					<th class="col-xs-1">#</th> 
					<th class="col-xs-6">Container Name</th> 
					<th class="col-xs-2">State</th>
					<th class="col-xs-2">Status</th> 
					<th class="col-xs-1"></th> 
				</tr> 
			</thead> 
			<tbody> 
				@foreach ($containers as $key => $container)
				<tr> 
					<th scope="row">{{ $key }}</th> 
					<td>
						<a tabindex="0" data-placement="bottom" data-toggle="popover" data-trigger="click" data-title="Container ID" data-html="true" data-content="{{ substr($container->getId(),0,12) }}">
							{{ substr($container->getNames()[0],1) }}
						</a>
					</td> 
					<td>
						@if($container->getState() == "running")
							<span class="label label-success">{{ $container->getState() }}</span>
						@else
							<span class="label label-danger">{{ $container->getState() }}</span>
						@endif
					</td> 
					<td>
						@if($container->getState() != "exited")
							{{ $container->getStatus() }}
						@endif
					</td>
					<td>
						
						<div class="btn-group">
					  		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					    		<span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>
					  		</button>
					  		<ul class="dropdown-menu pull-right">
							    
							    @if($container->getState() == "exited")
							    	<li><a href="{{ route('docker.run', ['action' =>'start','id' => $container->getId()])}}">Start container</a></li>
							    @endif
							    
							    @if($container->getState() == "running")
							    	<li><a href="{{ route('docker.run', ['action' =>'stop','id' => $container->getId()])}}">Stop container</a></li>
							    	<li><a href="{{ route('docker.run', ['action' =>'pause','id' => $container->getId()])}}">Pause container</a></li>
							    	<li><a href="{{ route('docker.run', ['action' =>'restart','id' => $container->getId()])}}">Restart container</a></li>
							    @endif
							    
							    @if( $container->getState() == "paused" )
							    	<li><a href="{{ route('docker.run', ['action' =>'unpause','id' => $container->getId()])}}">Unpause container</a></li>
						    	@endif

					  		</ul>
						</div>

					</td> 
				</tr> 
				@endforeach
			</tbody> 
		</table>
	</div>
@endsection
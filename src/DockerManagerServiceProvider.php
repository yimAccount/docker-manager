<?php

namespace yimOHG\DockerManager;

use Illuminate\Support\ServiceProvider;

class DockerManagerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (! $this->app->routesAreCached()) {
            require __DIR__.'/Http/routes.php';
        }

        $this->loadViewsFrom(__DIR__.'/resources/views', 'dockermanager');

        $this->publishes([
            __DIR__.'/resources/views' => resource_path('views/vendor/dockermanager'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
